import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'


Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Login',
        component: Login
    },
    {
        path: '/tables',
        name: 'Tables',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/Tables.vue')
    },
    {
        path: '/orders',
        name: 'Orders',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/Orders.vue')
    },
    {
        path: '/admin',
        name: 'Admin',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/Admin.vue')
    },
    {
        path: '/products',
        name: 'Products',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/Products.vue')
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router